package li.lambda.trafo;

import javafx.scene.control.TextArea;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

public class Buffer {

    private File file;
    private String name = "Unsaved-Buffer";

    //current text box content, not file content
    private String buffer = "";

    /**
     * Constructor for an empty buffer.
     */
    public Buffer() {
        //nop
    }

    public void setFile(final File f) throws Exception {
        this.file = f;
        reload();
    }

    /**
     * Save the given content to the file and 'reload' the internal buffer if successful.
     * @param content
     * @return true if saved
     */
    public boolean save(final String content) {
        boolean ok = false;
        if (this.file != null && this.file.canWrite()) {
            try {
                FileUtils.writeStringToFile(this.file, content, Utils.DEFAULT_CHARSET);
                this.buffer = content; //update the internal buffer too if file saved
                ok = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return ok;
    }

    public boolean reload() {
        boolean ok = false;
        if (this.file != null && this.file.exists() && this.file.canRead()) {
            try {
                this.buffer = FileUtils.readFileToString(this.file, Utils.DEFAULT_CHARSET);
                //System.out.println(this.buffer);
                this.name = this.file.getName();
                ok = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return ok;
    }

    public File getFile() {
        return file;
    }

    public String getBuffer() {
        return buffer;
    }

    public String getName() {
        return name;
    }
}
