package li.lambda.trafo.gui;


import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.net.URL;

/**
 * Main class
 */
public class Main extends Application {

    private FXMLLoader loader = null;

    @Override
    public void start(Stage primaryStage) {
        try {
            URL fxml = getClass().getResource("/fxml/Main.fxml");
            //System.out.println("URL = " + fxml.toExternalForm());
            // load the FXML resource
            loader = new FXMLLoader(fxml);
            // store the root element so that the controllers can use it
            BorderPane rootElement = (BorderPane) loader.load();
            // create and style a scene
            Scene scene = new Scene(rootElement, 720, 480);
            scene.getStylesheets().add(getClass().getResource("/fxml/application.css").toExternalForm());
            // create the stage with the given title and the previously created scene
            primaryStage.setTitle("XLS Trafo");
            primaryStage.getIcons().add(new Image(Main.class.getResourceAsStream("/icon.png")));
            primaryStage.setScene(scene);
            // show the GUI
            primaryStage.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void stop(){
        System.out.println("Good Bye.");
        if (loader != null) {
            MainController controller = loader.getController();
            controller.setClosed();
        }
    }

    /**
     * For launching the application...
     *
     * @param args optional params
     */
    public static void main(String[] args) {
        // launch the app
        launch(args);
    }

}