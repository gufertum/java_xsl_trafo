package li.lambda.trafo;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;

public class TestTransformation {

    @Test
    public void testTransformation() throws Exception {

        String expected = IOUtils.toString(this.getClass().getResource("/expected.xml"),Utils.DEFAULT_CHARSET);

        String xml = IOUtils.toString(this.getClass().getResource("/xml.xml"),Utils.DEFAULT_CHARSET);
        String xsl = IOUtils.toString(this.getClass().getResource("/xslt.xslt"),Utils.DEFAULT_CHARSET);

        String output = Trafo.transform(xml,xsl);

        //Use this to generate the transformed document for the test (in case you change the xslt or the xml)
        //FileUtils.writeStringToFile(new File ("src/test/resources/expected.xml"), output, Utils.DEFAULT_CHARSET);

        Assert.assertEquals(output.trim(),expected.trim());
    }

}
