<xsl:stylesheet version="2.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns="http://trafo.lambda.li/xslt"
>

    <xsl:strip-space elements="*" />
    <xsl:output method="xml" indent="yes" use-character-maps="angle-brackets" />

    <xsl:template match="/msg/body/items">
        <xsl:element name="root">
            <xsl:apply-templates select="item"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="item">
        <xsl:element name="element"><xsl:value-of select="id"/></xsl:element>
    </xsl:template>

    <!-- ignore unmatched text output -->
    <xsl:template match="text()"/>

</xsl:stylesheet>