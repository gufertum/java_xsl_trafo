## XSL Trafo

A very basic XSL(T) transformator to test stuff. Uses JavaFX with drag and drop.

Implemented features:

* XSL transformation (with log output)
* Error and stacktrace information
* One-click jar deployment and automatic build on bitbucket

Planned features:
* XPath tester
* Regex tester
* Editor support (basic XML/XSL support and highlighting)
* Stacktrace color highlighting
* ...

