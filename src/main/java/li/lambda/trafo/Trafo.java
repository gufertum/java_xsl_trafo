package li.lambda.trafo;

import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;

public class Trafo {

    private static final TransformerFactory factory = TransformerFactory.newInstance();


    private static Transformer getTransformer(Source xslt) throws TransformerConfigurationException {
        Transformer transformer = factory.newTransformer(xslt);
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
        return transformer;
    }

    public static String transform(File xml, File xsl) throws Exception {

        Source xslt = new StreamSource(xsl);
        Transformer transformer = getTransformer(xslt);

        StringWriter outWriter = new StringWriter();

        Source text = new StreamSource(xml);
        transformer.transform(text, new StreamResult(outWriter));

        return outWriter.toString();
    }

    public static String transform (String xml, String xsl) throws Exception {

        Source xslt = new StreamSource(new StringReader(xsl));
        Transformer transformer = getTransformer(xslt);

        StringWriter outWriter = new StringWriter();

        Source text = new StreamSource(new StringReader(xml));
        transformer.transform(text, new StreamResult(outWriter));

        return outWriter.toString();
    }
}
