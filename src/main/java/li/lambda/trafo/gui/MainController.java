package li.lambda.trafo.gui;


import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;

import javafx.scene.layout.StackPane;
import li.lambda.trafo.Buffer;
import li.lambda.trafo.Trafo;
import li.lambda.trafo.Utils;
import org.fxmisc.flowless.VirtualizedScrollPane;
import org.fxmisc.richtext.CodeArea;
import org.fxmisc.richtext.LineNumberFactory;
import org.fxmisc.richtext.model.StyleSpans;
import org.fxmisc.richtext.model.StyleSpansBuilder;

import java.awt.*;
import java.io.*;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.ResourceBundle;
import java.util.concurrent.ScheduledExecutorService;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The controller for our application, where the application logic is
 * implemented. It handles the button for starting/stopping the camera and the
 * acquired video stream.
 */
public class MainController implements Initializable {

    private static final boolean doResize = true;

    // the FXML button
    @FXML
    private Button button;
    // the FXML label to show the detected object counter
    @FXML
    private Label text;
    // the FXML image view
    @FXML
    private ImageView currentFrame;
    // the FXML label to show the status
    @FXML
    private Label status = new Label();
    //editors
    @FXML
    private CodeArea xml = new CodeArea();
    @FXML
    private CodeArea xsl = new CodeArea();
    @FXML
    private CodeArea log = new CodeArea();

    @FXML
    private Label nameXml = new Label();
    @FXML
    private Label nameXsl = new Label();

    //buffers to work with
    private Buffer xmlBuffer = new Buffer();
    private Buffer xslBuffer = new Buffer();

    // the last label to keep track of updates
    private String oldText = "";

    // target dimension for images (to display and calculate)
    private Dimension TARGET_DIMENSION = new Dimension(720, 500);

    //XML matching stuff for the xml editors
    private static final Pattern XML_TAG = Pattern.compile("(?<ELEMENT>(</?\\h*)(\\w+)([^<>]*)(\\h*/?>))|(?<COMMENT><!--[^<>]+-->)");
    private static final Pattern ATTRIBUTES = Pattern.compile("(\\w+\\h*)(=)(\\h*\"[^\"]+\")");
    private static final int GROUP_OPEN_BRACKET = 2;
    private static final int GROUP_ELEMENT_NAME = 3;
    private static final int GROUP_ATTRIBUTES_SECTION = 4;
    private static final int GROUP_CLOSE_BRACKET = 5;
    private static final int GROUP_ATTRIBUTE_NAME = 1;
    private static final int GROUP_EQUAL_SYMBOL = 2;
    private static final int GROUP_ATTRIBUTE_VALUE = 3;


    @Override // This method is called by the FXMLLoader when initialization is complete
    public void initialize(URL fxmlFileLocation, ResourceBundle resources) {

        //init the text areas
        initTextArea(xml, xmlBuffer, nameXml);
        initTextArea(xsl, xslBuffer, nameXsl);

        //init log area
        log.setParagraphGraphicFactory(LineNumberFactory.get(log));
        //set highlighting
        log.textProperty().addListener((obs, oldText, newText) -> {
            log.setStyleSpans(0, computeHighlighting(newText));
        });

        //update labels
        updateStatus("Welcome to XSL Trafo: start dragging and dropping xml/xsl(t) files!");
        updateLabel("");
    }

    private void initTextArea(final CodeArea codeArea, final Buffer buffer, final Label label) {

        //add line numbers
        codeArea.setParagraphGraphicFactory(LineNumberFactory.get(codeArea));

        //set highlighting
        codeArea.textProperty().addListener((obs, oldText, newText) -> {
            codeArea.setStyleSpans(0, computeHighlighting(newText));
        });

        //enable drag and drop for text
        codeArea.setOnDragOver(new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent event) {
                Dragboard db = event.getDragboard();
                if (event.getGestureSource() != codeArea && (db.hasFiles() || db.hasString())) {
                    event.acceptTransferModes(TransferMode.COPY);
                } else {
                    event.consume();
                }
            }
        });

        codeArea.setOnDragDropped((DragEvent event) -> {
            Dragboard db = event.getDragboard();
            boolean success = false;
            if (db.hasFiles()) {
                success = true;
                String filePath = null;
                for (File file:db.getFiles()) {
                    try {
                        //System.out.println("File dropped " + file.getAbsolutePath());
                        buffer.setFile(file);
                        label.setText(buffer.getName());
                        codeArea.replaceText(buffer.getBuffer());
                    } catch (Exception e) {
                        logException("File handling error", e);
                    }
                }
            } else if (db.hasString()) {
                codeArea.replaceText(db.getString());
                success = true;
            }
            event.setDropCompleted(success);
            event.consume();
        });
    }


    /**
     * Update the {@link ImageView} in the JavaFX main thread
     *
     * @param view  the {@link ImageView} to update
     * @param image the {@link Image} to show
     */
    private void updateImageView(ImageView view, Image image) {
        Utils.onFXThread(view.imageProperty(), image);
    }

    /**
     * The action triggered by pushing the button on the GUI
     * @param event the push button event
     */
    @FXML
    protected void transform(ActionEvent event) {
        //System.out.println("Button pressed: tansform");
        try {
            log.replaceText(Trafo.transform(xml.getText(),xsl.getText()));
            updateStatus("Transformation done @ " + System.nanoTime());
        } catch (Exception e) {
            logException("Transformation error", e);
        }
    }

    /**
     * Save the XML file.
     * @param event the push button event
     */
    @FXML
    protected void saveXml(ActionEvent event) {
        //System.out.println("Button pressed: saveXml");
        save(xmlBuffer, xml.getText());
    }

    /**
     * Save the XSL file.
     * @param event the push button event
     */
    @FXML
    protected void saveXsl(ActionEvent event) {
        //§System.out.println("Button pressed: saveXsl");
        save(xslBuffer, xsl.getText());
    }

    /**
     * Save the buffert to the file.
     * @param buf
     * @param content to save
     */
    private void save(Buffer buf, final String content) {
        if (buf != null) {
            boolean ok = buf.save(content);
            if (ok) {
                updateStatus("File saved: " + buf.getFile().getAbsolutePath());
            }
        }
    }

    /**
     * Reload the XML file.
     * @param event the push button event
     */
    @FXML
    protected void reloadXml(ActionEvent event) {
        //System.out.println("Button pressed: saveXml");
        reload(xmlBuffer, xml);
    }

    /**
     * Reload the XSL file.
     * @param event the push button event
     */
    @FXML
    protected void reloadXsl(ActionEvent event) {
        //§System.out.println("Button pressed: saveXsl");
        reload(xslBuffer, xsl);
    }

    /**
     * Reload the file.
     * @param buf
     */
    private void reload(final Buffer buf, final CodeArea textArea) {
        if (buf != null) {
            boolean ok = buf.reload();
            if (ok) {
                textArea.replaceText(buf.getBuffer());
                updateStatus("Reloaded file: " + buf.getFile().getAbsolutePath());
            }
        }
    }

    private void logException(final String text, final Exception e) {
        StringWriter errors = new StringWriter();
        e.printStackTrace(new PrintWriter(errors));
        log.replaceText(errors.toString());
        updateStatus(text + " @ " + System.nanoTime());
    }


    private void updateStatus(final String text) {
        if (text != null) {
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    status.setText(text);
                }
            });
        }
    }

    private void updateLabel(final String newText) {
        if (newText != null && !newText.equals(oldText)) {
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    oldText = newText;
                    text.setText(newText);
                }
            });
        }
    }

    /**
     * On application close, stop the acquisition from the camera
     */
    protected void setClosed() {
        System.out.println("Stop requested...");
    }


    /**
     * Styles for the codeArea / XML highligter
     * @param text
     * @return
     */
    private static StyleSpans<Collection<String>> computeHighlighting(String text) {

        Matcher matcher = XML_TAG.matcher(text);
        int lastKwEnd = 0;
        StyleSpansBuilder<Collection<String>> spansBuilder = new StyleSpansBuilder<>();
        while(matcher.find()) {

            spansBuilder.add(Collections.emptyList(), matcher.start() - lastKwEnd);
            if(matcher.group("COMMENT") != null) {
                spansBuilder.add(Collections.singleton("comment"), matcher.end() - matcher.start());
            }
            else {
                if(matcher.group("ELEMENT") != null) {
                    String attributesText = matcher.group(GROUP_ATTRIBUTES_SECTION);

                    spansBuilder.add(Collections.singleton("tagmark"), matcher.end(GROUP_OPEN_BRACKET) - matcher.start(GROUP_OPEN_BRACKET));
                    spansBuilder.add(Collections.singleton("anytag"), matcher.end(GROUP_ELEMENT_NAME) - matcher.end(GROUP_OPEN_BRACKET));

                    if(!attributesText.isEmpty()) {

                        lastKwEnd = 0;

                        Matcher amatcher = ATTRIBUTES.matcher(attributesText);
                        while(amatcher.find()) {
                            spansBuilder.add(Collections.emptyList(), amatcher.start() - lastKwEnd);
                            spansBuilder.add(Collections.singleton("attribute"), amatcher.end(GROUP_ATTRIBUTE_NAME) - amatcher.start(GROUP_ATTRIBUTE_NAME));
                            spansBuilder.add(Collections.singleton("tagmark"), amatcher.end(GROUP_EQUAL_SYMBOL) - amatcher.end(GROUP_ATTRIBUTE_NAME));
                            spansBuilder.add(Collections.singleton("avalue"), amatcher.end(GROUP_ATTRIBUTE_VALUE) - amatcher.end(GROUP_EQUAL_SYMBOL));
                            lastKwEnd = amatcher.end();
                        }
                        if(attributesText.length() > lastKwEnd)
                            spansBuilder.add(Collections.emptyList(), attributesText.length() - lastKwEnd);
                    }

                    lastKwEnd = matcher.end(GROUP_ATTRIBUTES_SECTION);

                    spansBuilder.add(Collections.singleton("tagmark"), matcher.end(GROUP_CLOSE_BRACKET) - lastKwEnd);
                }
            }
            lastKwEnd = matcher.end();
        }
        spansBuilder.add(Collections.emptyList(), text.length() - lastKwEnd);
        return spansBuilder.create();
    }
}
